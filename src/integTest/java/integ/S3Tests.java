package integ;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.skillbox.tests3.tests3.Tests3Application;
import com.skillbox.tests3.tests3.properties.S3Properties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Tests3Application.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class S3Tests {

    private final String filePath = "src/main/resources/images.jpeg";

    @Autowired
    private S3Properties s3Properties;

    @Container
    public static LocalStackContainer localstack = new LocalStackContainer(DockerImageName.parse("localstack/localstack:0.11.3"))
            .withServices(S3);

    AmazonS3 s3 = AmazonS3ClientBuilder
            .standard()
            .withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration(
                            localstack.getEndpoint().toString(),
                            localstack.getRegion()
                    )
            )
            .withCredentials(
                    new AWSStaticCredentialsProvider(
                            new BasicAWSCredentials(localstack.getAccessKey(), localstack.getSecretKey())
                    )
            )
            .build();

    @BeforeEach
    public void setup() {
        String bucketName = s3Properties.getBucketJpeg();

        if (!s3.doesBucketExistV2(bucketName)) {
            s3.createBucket(bucketName);
        }
    }


    @Test
    @Order(1)
    public void save_image() throws IOException {
        //given
        File file = new File(filePath);
        var stream = new FileInputStream(file);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(file.length());
        metadata.setContentType(String.valueOf(MediaType.IMAGE_JPEG));

        //when
        s3.putObject(s3Properties.getBucketJpeg(), file.getName(), stream, metadata);

        //then
        assertNotNull(s3.getObject(s3Properties.getBucketJpeg(), file.getName()));
        stream.close();
    }

    @Test
    @Order(2)
    public void delete_image() {
        //given
        File file = new File(filePath);
        //when
        s3.deleteObject(s3Properties.getBucketJpeg(), file.getName());
        //then
        assertThrows(AmazonS3Exception.class, () -> s3.getObject(s3Properties.getBucketJpeg(), file.getName()));
    }
}
