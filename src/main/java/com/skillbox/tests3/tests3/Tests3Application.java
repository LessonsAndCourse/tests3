package com.skillbox.tests3.tests3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tests3Application {

	public static void main(String[] args) {
		SpringApplication.run(Tests3Application.class, args);
	}
}
