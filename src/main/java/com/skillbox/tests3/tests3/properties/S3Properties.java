package com.skillbox.tests3.tests3.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "aws.s3")
@Data
public class S3Properties {
    private String endpoint;
    private String accessKey;
    private String secretKey;
    private String region;
    private String bucketJpeg;
    private String signer;
}
